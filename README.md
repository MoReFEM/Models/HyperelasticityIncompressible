A quasi incompressible hyperelastic case using the following assumptions:
- Midpoint time scheme.
- CiarletGeymonat hyperelastic law.

It must be noted a more generic implementation of hyperelastic model do exist in an independant model, in which you may choose half sum/midpoint and any hyperelastic law. It is however more challenging code to read, with arguably some not very user-friendly classes that hinder the understanding, hence the decision to implement simpler example for the incompressible formulation

These hyperelasticity models have been successfully run with MoReFEM v19.47.

# Compilation

- On a Mac, you may use the XCode workspace.

It currently assumes the following hierarchy:

    - Main library located at: ${HOME}/Codes/MoReFEM/CoreLibrary
    - And the current project at: ${HOME}/Codes/MoReFEM/Models/CardiacMechanics.
    
- A CMake build (called through a custom Python script)

    ````
    python ../../../CoreLibrary/cmake/Scripts/configure_cmake_external_model.py --morefem_install_dir=${MOREFEM_INSTALL_PREFIX} --cmake_args="-G Ninja"
    ninja -j 8
    ninja install
    ````

    where MOREFEM_INSTALL_PREFIX is the installation prefix used in the CMake installation of MoReFEM (and visible in the CMakeCache.txt file as `CMAKE_INSTALL_PREFIX`). This directory should include 4 subdirectories: _bin_, _cmake_, _include_ and _lib_.

    `ninja install` must have been called in MoReFEM.
    

# Tests

Integration tests that run the models and their subsequent conversion to Ensight format are run. Another test check the output is the same as the one recorded previously.

The tests are run through:

````
ninja test
````

or alternatively by:

````
ctest
````.

The latter is more powerful and may take some arguments:
    . -R to select a subset of the test. For instance `ctest -R Surfacic` will run only tests which name contains Surfacic.
    . -V to print on screen the output.