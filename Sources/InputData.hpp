/// \file
///
///
/// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 14 Nov 2013 13:42:29 +0100
/// Copyright (c) Inria. All rights reserved.
///

#ifndef MOREFEM_FOR_HYPERELASTICITY_INCOMPRESSIBLE_x_INPUT_DATA_HPP_
# define MOREFEM_FOR_HYPERELASTICITY_INCOMPRESSIBLE_x_INPUT_DATA_HPP_

# include "Utilities/Containers/EnumClass.hpp"

# include "Core/InputData/InputData.hpp"
# include "Core/InputData/Instances/Geometry/Domain.hpp"
# include "Core/InputData/Instances/Geometry/Mesh.hpp"
# include "Core/InputData/Instances/Geometry/PseudoNormals.hpp"
# include "Core/InputData/Instances/FElt/FEltSpace.hpp"
# include "Core/InputData/Instances/FElt/Unknown.hpp"
# include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
# include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"
# include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
# include "Core/InputData/Instances/Parameter/Source/Pressure.hpp"
# include "Core/InputData/Instances/Solver/Petsc.hpp"
# include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
# include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
# include "Core/InputData/Instances/Result.hpp"


namespace MoReFEM
{


    namespace HyperelasticityIncompressibleNS
    {


        //! \copydoc doxygen_hide_numbering_subset_enum
        enum class NumberingSubsetIndex
        {
            displacement = 1,
            pressure = 2,
            monolithic = 3
        };

        //! \copydoc doxygen_hide_unknown_enum
        enum class UnknownIndex
        {
            displacement = 1,
            pressure = 2
        };

        //! \copydoc doxygen_hide_mesh_enum
        enum class MeshIndex
        {
            mesh = 1
        };

        //! \copydoc doxygen_hide_domain_enum
        enum class DomainIndex
        {
            full_mesh = 1,
            volume = 2,
            force = 3,
            dirichlet = 4,
        };

        //! \copydoc doxygen_hide_boundary_condition_enum
        enum class BoundaryConditionIndex
        {
            clamped = 1
        };

        //! \copydoc doxygen_hide_felt_space_enum
            enum class FEltSpaceIndex
        {
            volume = 1,
            force = 2
        };


        //! \copydoc doxygen_hide_solver_enum
        enum class SolverIndex
        {
            solver = 1
        };

        //! \copydoc doxygen_hide_source_enum
        enum class ForceIndexList
        {
            surfacic = 1
        };

        //! \copydoc doxygen_hide_input_data
        using InputDataTuple = std::tuple
        <
            InputDataNS::TimeManager,

            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::pressure)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic)>,

            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::pressure)>,

            InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::force)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet)>,

            InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::clamped)>,

            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::force)>,

            InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

            InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic)>,

            InputDataNS::Solid::VolumicMass,
            InputDataNS::Solid::HyperelasticBulk,
            InputDataNS::Solid::Kappa1,
            InputDataNS::Solid::Kappa2,
            InputDataNS::Solid::LameLambda,
            InputDataNS::Solid::LameMu,
            InputDataNS::Solid::YoungModulus,
            InputDataNS::Solid::PoissonRatio,
            InputDataNS::Solid::C0,
            InputDataNS::Solid::C1,
            InputDataNS::Solid::C2,
            InputDataNS::Solid::C3,
            InputDataNS::Solid::Mu1,
            InputDataNS::Solid::Mu2,
			InputDataNS::Solid::CheckInvertedElements,

            InputDataNS::Result
        >;

        //! \copydoc doxygen_hide_model_specific_input_data
        using InputData = InputData<InputDataTuple>;

		//! \copydoc doxygen_hide_morefem_data_type
		using morefem_data_type = MoReFEMData<InputData, program_type::model>;


    } // namespace CardiacMechanicsPrestressNS


} // namespace MoReFEM


#endif // MOREFEM_FOR_HYPERELASTICITY_INCOMPRESSIBLE_x_INPUT_DATA_HPP_
